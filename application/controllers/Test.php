<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;

class Test extends CI_Controller {

	public function index()
	{

	}

	public function send_email()
	{
		$email = 'papaaldrin@gmail.com';
		$admin_email = 'devbctph@gmail.com';
		$link = '123';

		$config = array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'smtp.gmail.com',
		    'smtp_port' => '587',
		    // 'smtp_crypto' => 'tls',
		    'smtp_user' => 'devbctph@gmail.com',
		    'smtp_pass' => 'Go0gle1234',
		    'smtp_timeout' => '30',
		    'mailtype'  => 'html', 
		    'charset'   => 'utf-8'
		);

        // send email
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->to($email);
        $this->email->from($admin_email, 'Webmaster');
        $this->email->subject('Welcome to Blockchain Technology Philippines');
        // $email_content = $this->load->view('email_notification', array(
        //     'validate_link' => $link
        // ), true);
        // $this->email->message($email_content);
        $this->email->message('This is an email test.');
        // var_dump($email_content);

        $result = $this->email->send();
        var_dump($result);

        echo $this->email->print_debugger();
	}

	public function send_email_phpmailer()
	{
		// require "../demo/vendor/autoload.php";

		try {
			$mail = new PHPMailer(true);
			$mail->isSMTP();
			$mail->Host = 'smtp.gmail.com';
			$mail->SMTPAuth = true;
			$mail->Username = 'devbctph@gmail.com';
			$mail->Password = 'Go0gle1234';
			$mail->SMTPSecure = 'tls';
			$mail->Port = 587;

			$mail->setFrom('devbctph@gmail.com', 'Mailer');
			$mail->addAddress('papaaldrin@gmail.com', 'Joe User');     // Add a recipient

			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Here is the subject';
			$mail->Body    = 'This is the HTML message body <b>in bold!</b>';

			$mail->send();

			echo 'Message has been sent';
		} catch (Exception $e) {
		    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}
	}
}
