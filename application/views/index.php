<?php $this->load->view('inc/header'); ?>

<div class="content">
    <div class="section row">
        <div class="col-md-6 parent-img">
            <div class="img" style="background-image: url(<?php echo site_url() ?>public/img/img_1.jpg);"></div>
        </div>
        <div class="col-md-6 row text-xs-center middle-xs">
            <div class="section-content bounceIn animated">
                <?php if(!$islogin){ ?>
                
                <h2>Sample Header</h2>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat earum, optio quidem expedita odio soluta consectetur praesentium aliquam facere quas ullam sequi veritatis quasi repudiandae adipisci animi accusantium suscipit debitis!</p>
                
                <?php }else{ ?>
                
                <h2>Lorem Ipsum</h2>
                <p>blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah</p>                
                
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="section row">
        <div class="col-md-6 order-xs-2 order-md-1 row middle-xs">
            <div class="section-content bounceInLeft animated">
                <?php if(!$islogin){ ?>
                
                <h2>Sample Header</h2>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat earum, optio quidem expedita odio soluta consectetur praesentium aliquam facere quas ullam sequi veritatis quasi repudiandae adipisci animi accusantium suscipit debitis!</p>
                
                <?php }else{ ?>
                
                <h2>Lorem Ipsum</h2>
                <p>blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah</p>                
                
                <?php } ?>
            </div>
        </div>
        <div class="col-md-6 order-xs-1 order-md-2 parent-img">
            <div class="img" style="background-image: url(<?php echo site_url() ?>public/img/img_2.jpg);"></div>
        </div>
    </div>
    <div class="section row">
        <div class="col-md-6 parent-img">
            <div class="img" style="background-image: url(<?php echo site_url() ?>public/img/img_3.jpg);"></div>
        </div>
        <div class="col-md-6 row text-xs-right middle-xs">
            <div class="section-content bounceInRight animated">
                <?php if(!$islogin){ ?>
                
                <h2>Sample Header</h2>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat earum, optio quidem expedita odio soluta consectetur praesentium aliquam facere quas ullam sequi veritatis quasi repudiandae adipisci animi accusantium suscipit debitis!</p>
                
                <?php }else{ ?>
                
                <h2>Lorem Ipsum</h2>
                <p>blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah</p>                
                
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="section row">
        <div class="col-md-6 order-xs-2 order-md-1 row middle-xs">
            <div class="section-content">
                <?php if(!$islogin){ ?>
                
                <h2>Sample Header</h2>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat earum, optio quidem expedita odio soluta consectetur praesentium aliquam facere quas ullam sequi veritatis quasi repudiandae adipisci animi accusantium suscipit debitis!</p>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat earum, optio quidem expedita odio soluta consectetur praesentium aliquam facere quas ullam sequi veritatis quasi repudiandae adipisci animi accusantium suscipit debitis!</p>
                
                <?php }else{ ?>
                
                <h2>Lorem Ipsum</h2>
                <p>blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah</p>
                <p>blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah</p>                
                
                <?php } ?>
            </div>
        </div>
        <div class="col-md-6 order-xs-1 order-md-2 parent-img">
            <div class="img" style="background-image: url(<?php echo site_url() ?>public/img/img_2.jpg);"></div>
        </div>
    </div>
</div>

<?php $this->load->view('inc/footer'); ?>