<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>This is just a dummy page</title>
</head>
<body>

	<h1>TEST PAGE ONLY</h1>

	<ul>
		<li><a href="<?php echo site_url().'home' ?>">home</a></li>
		<li><a href="<?php echo $link ?>"><?php echo $link_label ?></a></li>
	</ul>
	
	<div class="content" style="font-size:16pt;">
		
		<?php echo $content ?>

	</div>

</body>
</html>