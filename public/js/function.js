jQuery(function($) {
    $(window).on('load scroll resize', function(event) {
        setScrollHeader();
    });
    
    function setScrollHeader() {
        var sTop = $(window).scrollTop();
        var hH = $('header').outerHeight(true);
    
        if($('body').hasClass('brand-top')) {
            hH = $('#waveCanvas').offset().top;
        }
    
        if(sTop > hH) {
            $('body').addClass('scroll');
        } else {
            $('body').removeClass('scroll');
        }
    }

    $( '.toggle-menu' ).on( 'click', function() {
        $( 'html' ).addClass( 'show-sidebar' );
        $( '.back-drop' ).show();
    });

    $( '.back-drop' ).on( 'click', function() {
        $( 'html' ).removeClass( 'show-sidebar' );
        $( this ).hide();
    });
});